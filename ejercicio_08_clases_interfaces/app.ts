// Crear interfaces

interface Auto {
  encender:boolean;
  velocidadMaxima:number;
  acelerar():void;
}
// Cree una interfaz para validar el auto (el valor enviado por parametro)
function conducirBatimovil( auto:Auto ):void{
  auto.encender = true;
  auto.velocidadMaxima = 100;
  auto.acelerar();
}

let batimovil:Auto = {
  encender:false,
  velocidadMaxima:0,
  acelerar(){
    console.log("...... run!!!");
  }
}

// Cree una interfaz con que permita utilzar el siguiente objeto
// utilizando propiedades opcionales

interface Persona {
  reir:boolean;
  comer?:boolean;
  llorar?:boolean;
  cantante?:boolean;
}

let guason:Persona = {
  reir: true,
  comer:true,
  llorar:false
}

function reir( guason:Persona ):void{
  if( guason.reir ){
    console.log("JAJAJAJA");
  }
}


// Cree una interfaz para la siguiente funcion


interface Longitud {
  (word:string[]): number
}

let ciudadGotica:Longitud;

ciudadGotica = function ( ciudadanos:string[] ):number{
  return ciudadanos.length;
}

// Cree una interfaz que obligue crear una clase



// con las siguientes propiedades y metodos

/*
  propiedades:
    - nombre
    - edad
    - sexo
    - estadoCivil
    - imprimirBio(): void // en consola una breve descripcion.
*/

interface People {

  nombre:string;
  edad:number;
  sexo:number; //1=hombre  2=mujer
  estadoCivil:string;
  imprimirBio():void;
}

class PeopleClass implements People {
  nombre:string;
  edad:number;
  sexo:number;
  estadoCivil:string;
  imprimirBio(){
    console.log("una breve descripcion.")
  }
}