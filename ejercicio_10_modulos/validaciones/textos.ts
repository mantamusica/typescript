const MENSAJES:string[] = [
    "Texto muy corto",
    "Texto muy largo",
];

function obtenerError(numError:number):string{
    return MENSAJES[numError];
}