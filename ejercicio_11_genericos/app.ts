function regresar<T>( arg:T) {
  return arg;
}

console.log(regresar(15.4646789).toFixed(2));
console.log(regresar("texto completo").length);
console.log(regresar(new Date().getTime));

function funcGenerica<T>(arg:T){
  return arg;
}

type Heroe = {
  nombre:string;
  nombreReal:string;
}

type Villano = {
  nombre: string;
  poder: string;
}

let deadpool = {
  nombre: "Deadpool",
  nombreReal: "Wade Winston Wilson",
  poder: "Regeneración"
};

console.log (funcGenerica<Heroe>(deadpool).nombreReal);

//Arreglo genérico
let heroes:Array<string> = ["Glace", "Tourbillon"];
heroes.push("123");

let villanos:string[] = ["lex luthor", "Flash"];

//Clases genéricos

class Cuadrado<T extends number|string> {
  base:T;
  altura:T;
  area():number{
    return +this.base * +this.altura;
  }
}

let cuadrado = new Cuadrado<number|string>();

cuadrado.base = '10';
cuadrado.altura = 10;
console.log(cuadrado.area());