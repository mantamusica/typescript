"use strict";
function regresar(arg) {
    return arg;
}
console.log(regresar(15.4646789).toFixed(2));
console.log(regresar("texto completo").length);
console.log(regresar(new Date().getTime));
function funcGenerica(arg) {
    return arg;
}
var deadpool = {
    nombre: "Deadpool",
    nombreReal: "Wade Winston Wilson",
    poder: "Regeneración"
};
console.log(funcGenerica(deadpool).nombreReal);
//Arreglo genérico
var heroes = ["Glace", "Tourbillon"];
heroes.push("123");
var villanos = ["lex luthor", "Flash"];
//Clases genéricos
var Cuadrado = /** @class */ (function () {
    function Cuadrado() {
    }
    Cuadrado.prototype.area = function () {
        return +this.base * +this.altura;
    };
    return Cuadrado;
}());
var cuadrado = new Cuadrado();
cuadrado.base = '10';
cuadrado.altura = 10;
console.log(cuadrado.area());
//# sourceMappingURL=app.js.map