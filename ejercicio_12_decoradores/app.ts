function planVillano( constructor:Function) {

  constructor.prototype.imprimirPlan = function(){
    console.log("El plan de " + this.nombre + " es dominar el mundo!");
  }
}

@planVillano
class Villano {

  constructor (public nombre:string ){

  }
}

let lex = new Villano("Lex Luthor");
lex.imprimirPlan();