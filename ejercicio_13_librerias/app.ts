import "jquery";
// ES6 Modules or TypeScript
import Swal from 'sweetalert/dist/sweetalert.min.js';

// CommonJS
const Swal = require('sweetalert')

$("#botAlerta").on("click", function(){
    
    $("h1").text("Hola desde Typescript.");
    $("p").text("Hola desde Typescript.");

    $("p").css("background-color", "red");
    Swal.pipe("Yeah", "Hola Mundo desde TypeScript", "success");
})


